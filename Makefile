CC = cc
PKG-CONFIG = `pkg-config --cflags --libs x11 xft freetype2 fontconfig`
CFLAGS = -Wall -pipe -O2
OUT-DIR = ./bin/
SRC-DIR = ./src/

${OUT-DIR}main.out: main.o win.o
	${CC} main.o win.o -o ${OUT-DIR}main.out ${CFLAGS} ${PKG-CONFIG}
	rm -f *.o

main.o:${SRC-DIR}main.c
	${CC} ${SRC-DIR}main.c -c ${CFLAGS} ${PKG-CONFIG}

win.o:${SRC-DIR}win.c
	${CC} ${SRC-DIR}win.c -c ${CFLAGS} ${PKG-CONFIG}

