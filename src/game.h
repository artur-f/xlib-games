#include <stdio.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xft/Xft.h>

void game_init();
void run();

typedef struct {
    Display *dpy;
    Colormap cmap;
    Window win;
    Drawable buffer;
    Visual *visual;
    XSetWindowAttributes attrs;
    int screen;
    int left, top;
    int width, height;
    int geometry_mask;
} GameWindow;

typedef struct {
    XftColor color;
    Font font;
    GC gc;
} GameDraw;

typedef struct {
    char *fontname;
    XftFont *font;
    XftDraw *draw;
} GameFont;

