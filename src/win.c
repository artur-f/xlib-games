#include "game.h"
#include <X11/Xft/Xft.h>

static GameWindow xwindow;
static GameDraw gd;
static GameFont gf;

void
game_init()
{
    XGCValues gc_values;
    Window parent;

    if (!(xwindow.dpy = XOpenDisplay(NULL)))
	printf("Can't open display");
    if (!FcInit())
	printf("could not init fontconfig.\n");
    xwindow.screen = XDefaultScreen(xwindow.dpy);
    xwindow.visual = XDefaultVisual(xwindow.dpy, xwindow.screen);
    xwindow.width = 800;
    xwindow.height = 600;
    xwindow.left += (XDisplayWidth(xwindow.dpy, xwindow.screen))/2;
    xwindow.top += (XDisplayHeight(xwindow.dpy, xwindow.screen))/2;

    parent = XRootWindow(xwindow.dpy, xwindow.screen);
    gc_values.graphics_exposures = 0;
    gc_values.foreground = 0xFFFFFF;

    xwindow.attrs.background_pixel = 0x000000;
    xwindow.attrs.event_mask = ButtonPressMask | ButtonReleaseMask |
	KeyPressMask | KeyReleaseMask |
	FocusChangeMask | ExposureMask;
    
    xwindow.win = XCreateWindow(xwindow.dpy,
			parent,
			xwindow.left,
			xwindow.top,
			xwindow.width,
			xwindow.height,
			0,
			XDefaultDepth(xwindow.dpy, xwindow.screen),
			InputOutput,
			xwindow.visual,
			CWBackPixel | CWEventMask, &xwindow.attrs);

    gd.gc = XCreateGC(xwindow.dpy,
		      xwindow.win,
		      GCGraphicsExposures | GCForeground,
		      &gc_values);

    XMapWindow(xwindow.dpy, xwindow.win);
    XSync(xwindow.dpy, False);
}

void
draw_toolbar(void)
{
    char *font = "InconsolataGo Nerd Font Mono";
    int toolbar_x;
    int toolbar_y;

    toolbar_x = 0;
    toolbar_y = 0;

    gf.fontname = font;

    XFillRectangle(xwindow.dpy,
		   xwindow.win,
		   gd.gc,
		   toolbar_x, toolbar_y,
		   xwindow.width, 25);
    
    gf.font = XftFontOpenName(xwindow.dpy, xwindow.screen, gf.fontname);
    gf.draw = XftDrawCreate(xwindow.dpy, xwindow.win, xwindow.visual, xwindow.cmap);

    XftDrawString8(gf.draw, 0xFF0000, gf.font, 0, 0, "", 10);
}

void
run(void)
{
    XEvent event;

    do {
	XNextEvent(xwindow.dpy, &event);

	switch (event.type) {
	case FocusIn:
	    draw_toolbar();
	}
    } while (event.type != KeyPress);
}



/* Window */
/* adsadda(Display* display, unsigned int width, unsigned int height) */
/* { */
/*     Window win; */
/*     long value_mask; */
/*     XSetWindowAttributes window_attr; */
/*     value_mask = CWBackPixel; */
/*     window_attr.background_pixel = 0xFF0000; */
/*     //window_attr.event_mask = ButtonPressMask; */

/*     win = XCreateWindow(display, */
/* 		  XDefaultRootWindow(display), */
/* 		  0, 0, */
/* 		  width, height, */
/* 		  5, */
/* 		  XDefaultDepth(display, XDefaultScreen(display)), */
/* 		  InputOutput, */
/* 		  XDefaultVisual(display, XDefaultScreen(display)), */
/* 		  value_mask, */
/* 		  &window_attr); */
    
/*     XSelectInput(display, win, ButtonPressMask | KeyPressMask | FocusChangeMask); */

/*     return win; */
/* }; */

/* GC */
/* create_simple_gc(Display* display, Window win) */
/* { */
/*     GC gc; */

/*     gc = XCreateGC(display, win, 0, NULL); */
    
/*     return gc; */
/* }; */

/* void */
/* create_toolbar(Display* display, Window win, GC gc, unsigned int width) */
/* { */
/*     int toolbar_x; */
/*     int toolbar_y; */
/*     char *pointer_string = "aaaaa"; */
/*     Font asd; */
/*     char *toolbar_opts[] = { */
/* 	"Game", */
/* 	"Keymaps" */
/*     }; */
/*     asd = XLoadFont(display, "InconsolataGo Nerd Font Mono"); */

/*     XTextItem toolbar_text; */
/*     toolbar_text.chars = pointer_string; */
/*     toolbar_text.font = asd; */
/*     toolbar_text.nchars = 5; */
/*     toolbar_text.delta = 2; */

/*     toolbar_x = 0; */
/*     toolbar_y = 0; */

/*     XFillRectangle(display, win, gc, toolbar_x, toolbar_y, width, 25); */

/*     XDrawText(display, win, gc, toolbar_x, (toolbar_y + 2), &toolbar_text, 1); */
/* }; */
